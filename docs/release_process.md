# Release Process

## Versioning

We follow [Semantic Versioning](https://semver.org).  In short, this means that the new version should reflect the types of changes that are about to be released.

*summary from semver.org*

MAJOR.MINOR.PATCH

- MAJOR version when you make incompatible API changes,
- MINOR version when you add functionality in a backwards compatible manner, and
- PATCH version when you make backwards compatible bug fixes.

## When we release

We release `gitlab-experiment` on an ad-hoc basis. There is no regularity to when we release, we simply release when the changes warrant a new release. This can be to fix a bug, or to implement new features and/or deprecate existing ones.

## How-to

To release, run `bin/release stage` locally.

This will trigger `#stage` under `lib/release/commands/stage_command.rb`. On your local machine, this phase will:
1. Create/checkout the release branch (`release-...`).
2. Validate if you updated the gem version.
3. Create changelog documentation.
4. Commit changes if needed.

Then the script pushes these changes to the repository and opens an MR to the default branch. **Avoid squashing commits** for that MR, to have a consistent git history in the default branch. Since release branches are protected, only maintainers are allowed to push these changes.

When the `release-...` branch appears in the repository, this triggers the same `#stage` method for `CI`, which opens an MR against the default branch. If the `push_to_gitlab` option is passed to the script, it also creates an MR in the [gitlab](https://gitlab.com/gitlab-org/gitlab) repository with the new gem version based on the release branch. This allows you to validate if tests pass.

When the release is approved by a maintainer, run `bin/release finalize` locally.

This will trigger `#finalize` under `lib/release/commands/finalize_command.rb`. On your local machine, this phase will:
1. Check out the release branch (`release-...`).
2. Create a new git tag `v<x>.<x>.<x>` and pushes it to the repository.

When a git tag `v<x>.<x>.<x>` appears in the [repository tags](https://gitlab.com/gitlab-org/ruby/gems/gitlab-experiment/-/tags), it triggers the same `#finalize` method but in `CI`. This does the following:
1. It builds the gem.
2. Pushes the new version to rubygems.
3. Creates a new [release](https://gitlab.com/gitlab-org/ruby/gems/gitlab-experiment/-/releases).
4. It accepts the MR in this repository from the previous step if it wasn't merged by a maintainer.
5. If the `push_to_gitlab` option is passed to the script, it updates the gem in an MR for [gitlab](https://gitlab.com/gitlab-org/gitlab) with the new gem version from rubygems.

And voila. You have successfuly released Gitlab Experiment gem.
