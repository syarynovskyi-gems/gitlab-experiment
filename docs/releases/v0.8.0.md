### 0.8.0 Changelog

- Make CI workflow rules more generic
- refactor: Cleanup rspec cleaner before hook
- fix: Correct typo in the initializer template
- Removes all deprecations from <= 0.7.0
- Disable fips linting
- Include DSL in ActionController::API
- fix: Require object/blank core extension
- Upgrade gitlab-styles to 9.0.0
- Bump ruby versions for test and general use
- CI: Add Ruby 3.2 to the matrix

#### Breaking Change Upgrade Path

Behaviors defined in experiment classes must be updated from:

```ruby
class MyExperiment < Gitlab::Experiment # OR ApplicationExperiment
  def control_behavior
  end

  def candidate_behavior
  end
end
```

To the following:

```ruby
class MyExperiment < Gitlab::Experiment # OR ApplicationExperiment
   control
   variant(:candidate)

   private

   def control_behavior
   end

   def candidate_behavior
   end
end
```
