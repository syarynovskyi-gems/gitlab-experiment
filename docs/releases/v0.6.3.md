### 0.6.3 Changelog

- Fixes an issue with generating urls when script_name is present in default_url_options
