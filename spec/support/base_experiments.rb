# frozen_string_literal: true

# Add in a reset! method that can be used in tests to reset an experiment back to a known state with the context. This
# restores an unresolved variant state, for a given context.
class Gitlab::Experiment
  def self.reset!
    %i[exclusion_check segmentation segmentation_skipped run run_skipped].each do |callback|
      reset_callbacks(callback)
    end

    # add the nestable callback again
    around_run(:manage_nested_stack)
  end

  def reset!(context = nil)
    @_assigned_variant_name = nil
    context(context) if context
    begin
      cache.delete
    rescue StandardError
      nil
    end
  end
end

# Define our base ApplicationExperiment, which we used to simulate when configuration specifies a different (and more
# like a Rails) value.
class ApplicationExperiment < Gitlab::Experiment
end

# We use this our testing of RSpec helpers and custom matchers, and may change this class in specs. Don't use this class
# outside of the rspec_spec.rb file.
class StubExperiment < Gitlab::Experiment
end

# An experiment that permits nested experiments. It's used to test that our nested experiment tracking module works and
# builds the correct structures.
class NestableExperiment < Gitlab::Experiment
  include Gitlab::Experiment::TestBehaviors::Trackable

  control

  def nest_experiment(_); end

  private

  def control_behavior; end
end

# We use this experiment (and the nested one) to test strict class definition and variant registration cases, where the
# full experiment definition is required.
class DefinedExperiment < Gitlab::Experiment
  control
  candidate { 'candidate' }

  private

  def control_behavior
    'control'
  end

  class InheritedExperiment < DefinedExperiment
    variant(:variant) { 'variant' }
  end
end

# We use this experiment to test variant registration cases.
class EmptyExperiment < DefinedExperiment
  private

  def control_method1
    'control_method1'
  end

  def control_method2
    'control_method2'
  end
end
