# frozen_string_literal: true

require 'spec_helper'

RSpec.describe "Rspec::Generators::ExperimentGenerator", :rails, type: :generator do
  tests { Rspec::Generators::ExperimentGenerator }
  destination { Rails.root.join('generated') }
  let(:args) { %w[NullHypothesis foo bar baz] }

  it "generates the spec file" do
    run_generator(args)

    expect(destination_root).to have_structure {
      file('spec/experiments/null_hypothesis_experiment_spec.rb') do
        contains <<~ERB
          # frozen_string_literal: true
          
          require 'rails_helper'
          
          RSpec.describe NullHypothesisExperiment do
            pending "add some examples to (or delete) \#{__FILE__}"
          end
        ERB
      end
    }
  end
end
