# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Gitlab::Experiment::Middleware do
  subject { described_class.new(app, '/base/path') }

  let(:app) { double(call: '_not_redirected_') }
  let(:env) do
    {
      'REQUEST_METHOD' => 'GET',
      'QUERY_STRING' => 'https://gitlab.com',
      'PATH_INFO' => '/base/path/namespace/name:abc123'
    }
  end

  context "when a request meets the criteria" do
    it "redirects" do
      expect(call_subject).to eq([303, { 'Location' => 'https://gitlab.com' }, []])
    end

    it "instantiates the right experiment class" do
      expect(config.logger).to receive(:info).with(/StubExperiment\[gitlab_experiment_stub\] visited:/)

      call_subject('PATH_INFO' => '/base/path/stub:abc123')
    end
  end

  context "when a request doesn't meet requirements" do
    it "ignores non GET requests" do
      expect(call_subject('REQUEST_METHOD' => 'POST')).to eq('_not_redirected_')
    end

    it "ignores things without query strings" do
      expect(call_subject('QUERY_STRING' => '')).to eq('_not_redirected_')
    end

    it "ignores paths that don't match our base path" do
      expect(call_subject('PATH_INFO' => 'namespace/name/abc123')).to eq('_not_redirected_')
    end
  end

  def call_subject(overrides = {})
    subject.call(env.merge(overrides))
  end
end
