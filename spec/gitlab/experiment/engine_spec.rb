# frozen_string_literal: true

require 'spec_helper'

RSpec.describe "Gitlab::Experiment::Engine", :rails do
  let(:initializers) { Gitlab::Experiment::Engine.initializers.map(&:name) }

  it "contains an initializer to include_dsl" do
    expect(initializers).to include('gitlab_experiment.include_dsl')
  end

  it "contains an initializer to mount_engine" do
    expect(initializers).to include('gitlab_experiment.mount_engine')
  end

  it "makes the base Experiment class understand ActiveModel naming" do
    model_name = Gitlab::Experiment.model_name
    expect(model_name).to be_a(ActiveModel::Name)
    expect(model_name.to_s).to eq('Gitlab::Experiment')
    expect(model_name.route_key).to eq('experiments')
  end

  describe ".include_dsl" do
    it "injects the DSL into ActionController" do
      expect(ActionController::API.instance_methods).to include(:experiment)
      expect(ActionController::Base.instance_methods).to include(:experiment)
      expect(ActionController::Base._helper_methods).to include(:experiment)
    end

    it "injects the DSL into ActionMailer" do
      expect(ActionMailer::Base.instance_methods).to include(:experiment)
      expect(ActionMailer::Base._helper_methods).to include(:experiment)
    end
  end

  describe ".mount_engine" do
    let(:app_url_helpers) { Rails.application.routes.url_helpers }
    let(:engine_url_helpers) { Gitlab::Experiment::Engine.routes.url_helpers }
    let(:subject_experiment) { experiment(:example) }

    it "adds the middleware" do
      expect(Rails.application.middleware).to include(Gitlab::Experiment::Middleware)
    end

    it "mounts the engine at the configured path" do
      expect(app_url_helpers.experiment_engine_path).to eq('/glex')
    end

    it "defines the direct redirect helpers (url and path)" do
      url = app_url_helpers.experiment_redirect_url(subject_experiment, url: 'https://example.com')
      expect(url).to eq("http://localhost:3000/glex/#{subject_experiment.to_param}?https://example.com")

      path = app_url_helpers.experiment_redirect_path(subject_experiment, url: 'https://example.com')
      expect(path).to eq("/glex/#{subject_experiment.to_param}?https://example.com")
    end

    it "defines resources for experiments" do
      path = engine_url_helpers.experiment_path(subject_experiment)
      expect(path).to eq("/glex/#{subject_experiment.to_param}")
    end
  end
end
